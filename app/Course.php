<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'name', 'code', 'prerequisite_id', 'text',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_course');
    }

}
