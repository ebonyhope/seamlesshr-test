<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\User;
use Auth;

class AuthController extends Controller
{


    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => 'required|string|min:8'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'success' => false,
                'data' => $validator->errors()
            ]);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        
        return response()->json([
            'status' => 200,
            'success' => true,
            'data' => $user
        ]);

    }


    public function login(Request $request)
    {

        $credentials = $request->only(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json([
                'status' => 401,
                'success' => false,
                'data' => ['message' => "Unauthorized Attempt"]
            ]);
        }

        return $this->respondWithToken($token);

    }


    public function refreshToken()
    {

        return $this->respondWithToken(auth()->refresh());

    }


    public function getAuthUser()
    {

        return response()->json(auth()->user());

    }


    public function logout()
    {

        auth()->logout();
        return response()->json([
            'status' => 200,
            'success' => true,
            'data' => ['message' => "Successfully logged out"]
        ]);

    }


    protected function respondWithToken($token)
    {
        return response()->json([
            'status' => 200,
            'success' => true,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

}
