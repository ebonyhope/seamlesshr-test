<?php

namespace App\Http\Controllers;

use App\Course;
use App\Exports\CourseExport;
use App\User;
use App\UserCourse;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;


class CourseController extends Controller
{

    public function register(Request $request) {

        $validator = Validator::make($request->all(), [
            'course_ids' => 'required|array'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'success' => false,
                'data' => $validator->errors()
            ]);
        }

        $user = auth()->user();
        $course_ids = $request->course_ids;

        $user->courses()->sync($course_ids);

        return response()->json([
            'status' => 200,
            'success' => true,
            'data' => $user->courses
        ]);

    }


    public function getAll()
    {
        $user_courses = array();

        foreach(auth()->user()->courses as $course){
            $user_courses[$course->pivot->course_id] = $course->pivot->created_at;
        }

        $courses = Course::all();

        foreach ($courses as $course) {
            if(array_key_exists($course->id, $user_courses)){
                $course->date_enrolled = $user_courses[$course->id];
            }
        }

        return response()->json([
            'status' => 200,
            'success' => true,
            'data' => $courses
        ]);
    }


    public function create() 
    {
        $num = 50;
        $courses = factory(Course::class, $num)->create();
        
        return response()->json([
            'status' => 200,
            'success' => true,
            'data' => ["message" => "$num courses created successfully"]
        ]);
    }


    public function export($type) 
    {
        if($type != "xlsx"){
            $type = "csv";
        }

        return Excel::download(new CourseExport, "courses".@date('d-M-Y-H-i-s').".".$type);
    }
}

?>